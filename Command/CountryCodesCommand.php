<?php
/**
 * @author        Rafal Przetakowski <rafal.p@beeflow.co.uk>
 * @copyright (c) 2016 Beeflow Ltd
 * Time: 16:56
 */

namespace Beeflow\BeeflowCountryCodesBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\LockHandler;
use Beeflow\BeeflowCountryCodesBundle\Entity\CountryCodes;
use Beeflow\JsonManager\JsonManager;

class CountryCodesCommand extends ContainerAwareCommand
{
    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var string
     */
    private $countryCodesService = "https://pkgstore.datahub.io/core/country-codes/latest/data/json/data/country-codes.json";

    protected function configure()
    {
        $this->setName('transfer:countries:all')->setDescription('Transfer list of countries');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $lock = new LockHandler('transfer:countries:all');
        if (!$lock->lock()) {
            $output->writeln('The command is already running in another process.');

            return 0;
        }

        $this->logger = $this->getContainer()->get('logger');
        $jsonData = $this->getJsonData();

        if ($jsonData == null) {
            return -1;
        }

        $this->insertIntoDatabase($jsonData);

        $this->logger->notice("Transfer finished");
    }

    /**
     * @param JsonManager $json
     */
    private function insertIntoDatabase(JsonManager $json)
    {

        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();
        $this->logger->notice("Prepering " . $json->length() . " records");
        $records = 0;
        foreach ($json as $element) {
            $elementOfficialName = $element->get('official_name_en');
            $elementName = $element->get('name');
            $name = (empty($elementOfficialName)) ? $elementName : $elementOfficialName;
            $this->logger->notice("[" . (++$records) . "/" . $json->length() . "] Prepering data for " . $name);
            $alpha3 = $element->get('M49');

            if (empty($alpha3)) {
                $this->logger->warning('Empty element');

                continue;
            }

            $cc = $this->getCountryCode($doctrine, $alpha3);
            $cc->setName($name);
            $cc->setNameFr($element->get('official_name_fr'));
            $cc->setIso31661Alpha2($element->get('ISO3166-1-Alpha-2'));
            $cc->setIso31661Alpha3($element->get('ISO3166-1-Alpha-3'));
            $cc->setMfn($element->get('M49'));
            $cc->setItu($element->get('ITU'));
            $cc->setMarc($element->get('MARC'));
            $cc->setWmo($element->get('WMO'));
            $cc->setDs($element->get('DS'));
            $cc->setDial($element->get('Dial'));
            $cc->setFifa($element->get('FIFA'));
            $cc->setFips($element->get('FIPS'));
            $cc->setGaul($element->get('GAUL'));
            $cc->setIoc($element->get('IOC'));
            $cc->setCurrencyAlphabeticCode($element->get('ISO4217-currency_alphabetic_code'));
            $cc->setCurrencyCountryName($element->get('ISO4217-currency_country_name'));
            $cc->setCurrencyMinorUnit((integer)$element->get('ISO4217-currency_minor_unit'));
            $cc->setCurrencyName($element->get('ISO4217-currency_name'));
            $cc->setCurrencyNumericCode((integer)$element->get('ISO4217-currency_numeric_code'));
            $cc->setIsIndependent($element->get('is_independent'));
            $cc->setCapital($element->get('Capital'));
            $cc->setContinent($element->get('Continent'));
            $cc->setTld($element->get('TLD'));
            $cc->setLanguages($element->get('Languages'));
            $cc->setGeonameId((integer)$element->get('Geoname ID'));
            $cc->setEdgar($element->get('EDGAR'));
            $em->merge($cc);
        }
        $this->logger->notice("Flushing records...");
        $em->flush();
        $this->logger->notice("Records flushed");
    }


    /**
     * @param $doctrine
     * @param $Iso31661Alpha3
     *
     * @return CountryCodes
     */
    private function getCountryCode(&$doctrine, $Iso31661Alpha3)
    {
        $element = $doctrine->getRepository('BeeflowCountryCodesBundle:CountryCodes')->findOneBy(['mfn' => $Iso31661Alpha3]);
        if (!empty($element)) {
            return $element;
        } else {
            return new CountryCodes();
        }
    }

    /**
     * @return JsonManager
     */
    private function getJsonData()
    {
        $response = file_get_contents($this->countryCodesService);

        if (empty($response)) {
            $this->logger->error($this->countryCodesService . ' respond nothing');

            return null;
        }

        return new JsonManager($response, true, 'mfn');
    }
}

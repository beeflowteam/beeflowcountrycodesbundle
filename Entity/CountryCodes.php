<?php
/**
 * @author        Rafal Przetakowski <rafal.p@beeflow.co.uk>
 * @copyright (c) 2016 Beeflow Ltd
 * Time: 14:36
 */

namespace Beeflow\BeeflowCountryCodesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;

/**
 * @ORM\Entity
 * @ORM\Table(name="country_codes")
 */
class CountryCodes
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=250, nullable=false, options={"comment":"Country's official English short name"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=250, nullable=true, options={"comment":"Country's offical French short name"})
     */
    private $name_fr;

    /**
     * @ORM\Column(type="string", length=2, nullable=true, options={"comment":"Alpha-2 codes from ISO 3166-1"})
     */
    private $iso3166_1_alpha_2;

    /**
     * @ORM\Column(type="string", length=3, nullable=true, options={"comment":"Alpha-3 codes from ISO 3166-1 (synonymous with World Bank Codes)"})
     */
    private $iso3166_1_alpha_3;

    /**
     * @ORM\Column(type="string", length=3, nullable=false, options={"comment":"M49"})
     */
    private $mfn;

    /**
     * @ORM\Column(type="string", length=3, nullable=true, options={"comment":"Codes assigned by the International Telecommunications Union"})
     */
    private $itu;

    /**
     * @ORM\Column(type="string", length=250, nullable=true, options={"comment":"MAchine-Readable Cataloging codes from the Library of Congress"})
     */
    private $marc;

    /**
     * @ORM\Column(type="string", length=2, nullable=true, options={"comment":"Country abbreviations by the World Meteorological Organization"})
     */
    private $wmo;

    /**
     * @ORM\Column(type="string", length=4, nullable=true, options={"comment":"Distinguishing signs of vehicles in international traffic"})
     */
    private $ds;

    /**
     * @ORM\Column(type="string", length=25, nullable=true, options={"comment":"Country code from ITU-T recommendation E.164, sometimes followed by area code"})
     */
    private $dial;

    /**
     * @ORM\Column(type="string", length=250, nullable=true, options={"comment":"Codes assigned by the Fédération Internationale de Football Association"})
     */
    private $fifa;

    /**
     * @ORM\Column(type="string", length=250, nullable=true, options={"comment":"Codes from the U.S. standard FIPS PUB 10-4"})
     */
    private $fips;

    /**
     * @ORM\Column(type="string", length=250, nullable=true, options={"comment":"Global Administrative Unit Layers from the Food and Agriculture Organization"})
     */
    private $gaul;

    /**
     * @ORM\Column(type="string", length=4, nullable=true, options={"comment":"Codes assigned by the International Olympics Committee"})
     */
    private $ioc;

    /**
     * @ORM\Column(type="string", length=3, nullable=true, options={"comment":"ISO 4217 currency alphabetic code"})
     */
    private $currency_alphabetic_code;

    /**
     * @ORM\Column(type="string", length=250, nullable=true, options={"comment":"ISO 4217 country name"})
     */
    private $currency_country_name;

    /**
     * @ORM\Column(type="integer", length=1, nullable=true, options={"comment":"ISO 4217 currency number of minor units"})
     */
    private $currency_minor_unit;

    /**
     * @ORM\Column(type="string", length=250, nullable=true, options={"comment":"ISO 4217 currency name"})
     */
    private $currency_name;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"comment":"ISO 4217 currency numeric code"})
     */
    private $currency_numeric_code;

    /**
     * @ORM\Column(type="string", length=250, nullable=true, options={"comment":"Country status, based on the CIA World Factbook"})
     */
    private $is_independent;

    /**
     * @ORM\Column(type="string", length=250, nullable=true, options={"comment":"Capital"})
     */
    private $capital;

    /**
     * @ORM\Column(type="string", length=3, nullable=true, options={"comment":"Continent code"})
     */
    private $continent;

    /**
     * @ORM\Column(type="string", length=10, nullable=true, options={"comment":"TLD"})
     */
    private $tld;

    /**
     * @ORM\Column(type="string", length=250, nullable=true, options={"comment":"languages"})
     */
    private $languages;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"comment":"Geoname ID"})
     */
    private $geoname_id;

    /**
     * @ORM\Column(type="string", length=250, nullable=true, options={"comment":"EDGAR"})
     */
    private $edgar;

    public function __toString()
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return CountryCodes
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name_fr
     *
     * @param string $nameFr
     *
     * @return CountryCodes
     */
    public function setNameFr($nameFr)
    {
        $this->name_fr = $nameFr;

        return $this;
    }

    /**
     * Get name_fr
     *
     * @return string
     */
    public function getNameFr()
    {
        return $this->name_fr;
    }

    /**
     * Set iso3166_1_alpha_2
     *
     * @param string $iso31661Alpha2
     *
     * @return CountryCodes
     */
    public function setIso31661Alpha2($iso31661Alpha2)
    {
        $this->iso3166_1_alpha_2 = $iso31661Alpha2;

        return $this;
    }

    /**
     * Get iso3166_1_alpha_2
     *
     * @return string
     */
    public function getIso31661Alpha2()
    {
        return $this->iso3166_1_alpha_2;
    }

    /**
     * Set iso3166_1_alpha_3
     *
     * @param string $iso31661Alpha3
     *
     * @return CountryCodes
     */
    public function setIso31661Alpha3($iso31661Alpha3)
    {
        $this->iso3166_1_alpha_3 = $iso31661Alpha3;

        return $this;
    }

    /**
     * Get iso3166_1_alpha_3
     *
     * @return string
     */
    public function getIso31661Alpha3()
    {
        return $this->iso3166_1_alpha_3;
    }

    /**
     * Set itu
     *
     * @param string $itu
     *
     * @return CountryCodes
     */
    public function setItu($itu)
    {
        $this->itu = $itu;

        return $this;
    }

    /**
     * Get itu
     *
     * @return string
     */
    public function getItu()
    {
        return $this->itu;
    }

    /**
     * Set marc
     *
     * @param string $marc
     *
     * @return CountryCodes
     */
    public function setMarc($marc)
    {
        $this->marc = $marc;

        return $this;
    }

    /**
     * Get marc
     *
     * @return string
     */
    public function getMarc()
    {
        return $this->marc;
    }

    /**
     * Set wmo
     *
     * @param string $wmo
     *
     * @return CountryCodes
     */
    public function setWmo($wmo)
    {
        $this->wmo = $wmo;

        return $this;
    }

    /**
     * Get wmo
     *
     * @return string
     */
    public function getWmo()
    {
        return $this->wmo;
    }

    /**
     * Set ds
     *
     * @param string $ds
     *
     * @return CountryCodes
     */
    public function setDs($ds)
    {
        $this->ds = $ds;

        return $this;
    }

    /**
     * Get ds
     *
     * @return string
     */
    public function getDs()
    {
        return $this->ds;
    }

    /**
     * Set dial
     *
     * @param string $dial
     *
     * @return CountryCodes
     */
    public function setDial($dial)
    {
        $this->dial = $dial;

        return $this;
    }

    /**
     * Get dial
     *
     * @return string
     */
    public function getDial()
    {
        return $this->dial;
    }

    /**
     * Set fifa
     *
     * @param string $fifa
     *
     * @return CountryCodes
     */
    public function setFifa($fifa)
    {
        $this->fifa = $fifa;

        return $this;
    }

    /**
     * Get fifa
     *
     * @return string
     */
    public function getFifa()
    {
        return $this->fifa;
    }

    /**
     * Set fips
     *
     * @param string $fips
     *
     * @return CountryCodes
     */
    public function setFips($fips)
    {
        $this->fips = $fips;

        return $this;
    }

    /**
     * Get fips
     *
     * @return string
     */
    public function getFips()
    {
        return $this->fips;
    }

    /**
     * Set gaul
     *
     * @param string $gaul
     *
     * @return CountryCodes
     */
    public function setGaul($gaul)
    {
        $this->gaul = $gaul;

        return $this;
    }

    /**
     * Get gaul
     *
     * @return string
     */
    public function getGaul()
    {
        return $this->gaul;
    }

    /**
     * Set ioc
     *
     * @param string $ioc
     *
     * @return CountryCodes
     */
    public function setIoc($ioc)
    {
        $this->ioc = $ioc;

        return $this;
    }

    /**
     * Get ioc
     *
     * @return string
     */
    public function getIoc()
    {
        return $this->ioc;
    }

    /**
     * Set currency_alphabetic_code
     *
     * @param string $currencyAlphabeticCode
     *
     * @return CountryCodes
     */
    public function setCurrencyAlphabeticCode($currencyAlphabeticCode)
    {
        $this->currency_alphabetic_code = $currencyAlphabeticCode;

        return $this;
    }

    /**
     * Get currency_alphabetic_code
     *
     * @return string
     */
    public function getCurrencyAlphabeticCode()
    {
        return $this->currency_alphabetic_code;
    }

    /**
     * Set currency_country_name
     *
     * @param string $currencyCountryName
     *
     * @return CountryCodes
     */
    public function setCurrencyCountryName($currencyCountryName)
    {
        $this->currency_country_name = $currencyCountryName;

        return $this;
    }

    /**
     * Get currency_country_name
     *
     * @return string
     */
    public function getCurrencyCountryName()
    {
        return $this->currency_country_name;
    }

    /**
     * Set currency_minor_unit
     *
     * @param integer $currencyMinorUnit
     *
     * @return CountryCodes
     */
    public function setCurrencyMinorUnit($currencyMinorUnit)
    {
        $this->currency_minor_unit = $currencyMinorUnit;

        return $this;
    }

    /**
     * Get currency_minor_unit
     *
     * @return integer
     */
    public function getCurrencyMinorUnit()
    {
        return $this->currency_minor_unit;
    }

    /**
     * Set currency_name
     *
     * @param string $currencyName
     *
     * @return CountryCodes
     */
    public function setCurrencyName($currencyName)
    {
        $this->currency_name = $currencyName;

        return $this;
    }

    /**
     * Get currency_name
     *
     * @return string
     */
    public function getCurrencyName()
    {
        return $this->currency_name;
    }

    /**
     * Set currency_numeric_code
     *
     * @param integer $currencyNumericCode
     *
     * @return CountryCodes
     */
    public function setCurrencyNumericCode($currencyNumericCode)
    {
        $this->currency_numeric_code = $currencyNumericCode;

        return $this;
    }

    /**
     * Get currency_numeric_code
     *
     * @return integer
     */
    public function getCurrencyNumericCode()
    {
        return $this->currency_numeric_code;
    }

    /**
     * Set is_independent
     *
     * @param string $isIndependent
     *
     * @return CountryCodes
     */
    public function setIsIndependent($isIndependent)
    {
        $this->is_independent = $isIndependent;

        return $this;
    }

    /**
     * Get is_independent
     *
     * @return string
     */
    public function getIsIndependent()
    {
        return $this->is_independent;
    }

    /**
     * Set capital
     *
     * @param string $capital
     *
     * @return CountryCodes
     */
    public function setCapital($capital)
    {
        $this->capital = $capital;

        return $this;
    }

    /**
     * Get capital
     *
     * @return string
     */
    public function geCapital()
    {
        return $this->capital;
    }

    /**
     * Set continent
     *
     * @param string $continent
     *
     * @return CountryCodes
     */
    public function setContinent($continent)
    {
        $this->continent = $continent;

        return $this;
    }

    /**
     * Get continent
     *
     * @return string
     */
    public function geContinent()
    {
        return $this->continent;
    }

    /**
     * Set tld
     *
     * @param string $tld
     *
     * @return CountryCodes
     */
    public function setTld($tld)
    {
        $this->tld = $tld;

        return $this;
    }

    /**
     * Get tld
     *
     * @return string
     */
    public function geTld()
    {
        return $this->tld;
    }

    /**
     * Set languages
     *
     * @param string $languages
     *
     * @return CountryCodes
     */
    public function setLanguages($languages)
    {
        $this->languages = $languages;

        return $this;
    }

    /**
     * Get string
     *
     * @return integer
     */
    public function geLanguages()
    {
        return $this->languages;
    }

    /**
     * Set geoname_id
     *
     * @param integer $geoname_id
     *
     * @return CountryCodes
     */
    public function setGeonameId($geoname_id)
    {
        $this->geoname_id = $geoname_id;

        return $this;
    }

    /**
     * Get geoname_id
     *
     * @return integer
     */
    public function geGeonameId()
    {
        return $this->geoname_id;
    }

   /**
     * Set edgar
     *
     * @param string $edgar
     *
     * @return CountryCodes
     */
    public function setEdgar($edgar)
    {
        $this->edgar = $edgar;

        return $this;
    }

    /**
     * Get edgar
     *
     * @return string
     */
    public function geEdgar()
    {
        return $this->edgar;
    }

    /**
     * Set mfn
     *
     * @param string $mfn
     *
     * @return CountryCodes
     */
    public function setMfn($mfn)
    {
        $this->mfn = $mfn;

        return $this;
    }

    /**
     * Get mfn
     *
     * @return string
     */
    public function geMfnr()
    {
        return $this->mfn;
    }
}

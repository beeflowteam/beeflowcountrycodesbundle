### BeeflowCountryCodesBundle ###
Jest to paczka zawierająca encję pozwalającę na szybkie podłączenie do innych encji kodów krajów i pobieranie ich do bazy z poziomu polecenia.

### Instalacja i konfiguracja paczki ###
Aby zainstalować pakiet należy dodać do composer.json:

    "repositories": [
        {
            "type": "vcs",
            "url":  "https://beeflow@bitbucket.org/beeflowteam/beeflowcountrycodesbundle.git"
        }
    ],
    "require": {
        ...
        "beeflow/beeflowcountrycodesbundle": "dev-master"
    }

i wykonać polecenie

    $ composer update
    
### Konfiguracja ###
Do pliku `app/config/config.yml` należy dodać:

    imports:
        ...
        - { resource: "@BeeflowCountryCodesBundle/Resources/config/services.yml" }
        
### Uruchomienie polecenia ###
Przed pierwszym uruchomieniem koniecznie wydaj polecenie 
                              
    php app/console doctrine:schema:update --force
      
potem możesz uruchomić transfer

    php app/console transfer:countries:all
    

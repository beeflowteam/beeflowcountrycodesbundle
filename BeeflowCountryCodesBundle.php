<?php
/**
 * @author   Rafal Przetakowski <rafal.p@beeflow.co.uk>
 * @copyright: (c) 2016 Beeflow Ltd
 *
 * Date: 12.09.16 10:05
 */

namespace Beeflow\BeeflowCountryCodesBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class BeeflowCountryCodesBundle extends Bundle
{

}